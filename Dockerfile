FROM caddy:2.0.0-builder AS builder

RUN export GO111MODULE=on
RUN caddy-builder \
    github.com/lucaslorentz/caddy-docker-proxy/plugin

FROM caddy:2.0.0

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
